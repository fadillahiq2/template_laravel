-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 28, 2021 at 08:44 PM
-- Server version: 10.4.13-MariaDB
-- PHP Version: 7.4.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `migration_laravel`
--

-- --------------------------------------------------------

--
-- Table structure for table `answers`
--

CREATE TABLE `answers` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `isi` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tanggal_dibuat` date NOT NULL,
  `tanggal_diubah` date NOT NULL,
  `pertanyaan_id` bigint(20) UNSIGNED NOT NULL,
  `profil_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `comments_answers`
--

CREATE TABLE `comments_answers` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `isi` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tanggal_dibuat` date NOT NULL,
  `jawaban_id` bigint(20) UNSIGNED NOT NULL,
  `profil_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `comments_questions`
--

CREATE TABLE `comments_questions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `isi` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tanggal_dibuat` date NOT NULL,
  `pertanyaan_id` bigint(20) UNSIGNED NOT NULL,
  `profil_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `like_dislike_answers`
--

CREATE TABLE `like_dislike_answers` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `poin` int(11) NOT NULL,
  `jawaban_id` bigint(20) UNSIGNED NOT NULL,
  `profil_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `like_dislike_questions`
--

CREATE TABLE `like_dislike_questions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `poin` int(11) NOT NULL,
  `pertanyaan_id` bigint(20) UNSIGNED NOT NULL,
  `profil_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(31, '2014_10_12_000000_create_users_table', 1),
(32, '2014_10_12_100000_create_password_resets_table', 1),
(33, '2019_08_19_000000_create_failed_jobs_table', 1),
(89, '2021_01_28_185339_create_profiles_table', 2),
(90, '2021_01_28_185356_create_questions_table', 2),
(91, '2021_01_28_185411_create_answers_table', 2),
(92, '2021_01_28_185428_create_comments_questions_table', 2),
(93, '2021_01_28_185449_create_comments_answers_table', 2),
(94, '2021_01_28_185758_create_like_dislike_answers_table', 2),
(95, '2021_01_28_185821_create_like_dislike_questions_table', 2),
(96, '2021_01_28_190611_add_jawaban_tepat_id_to_questions', 2),
(97, '2021_01_28_190835_add_profil_id_to_questions', 2),
(98, '2021_01_28_191222_add_pertanyaan_id_to_answers', 2),
(99, '2021_01_28_191240_add_profil_id_to_answers', 2),
(100, '2021_01_28_191657_add_pertanyaan_id_to_comments_questions', 2),
(101, '2021_01_28_191721_add_profil_id_to_comments_questions', 2),
(102, '2021_01_28_192146_add_jawaban_id_to_comments_answers', 2),
(103, '2021_01_28_192210_add_profil_id_to_comments_answers', 2),
(104, '2021_01_28_193115_add_pertanyaan_id_to_like_dislike_questions', 2),
(105, '2021_01_28_193130_add_profil_id_to_like_dislike_questions', 2),
(106, '2021_01_28_193540_add_jawaban_id_to_like_dislike_answers', 2),
(107, '2021_01_28_193551_add_profil_id_to_like_dislike_answers', 2);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `profiles`
--

CREATE TABLE `profiles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nama_lengkap` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(45) COLLATE utf8mb4_unicode_ci NOT NULL,
  `foto` varchar(45) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `questions`
--

CREATE TABLE `questions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `judul` varchar(45) COLLATE utf8mb4_unicode_ci NOT NULL,
  `isi` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tanggal_dibuat` date NOT NULL,
  `tanggal_diperbaharui` date NOT NULL,
  `jawaban_tepat_id` bigint(20) UNSIGNED NOT NULL,
  `profil_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `answers`
--
ALTER TABLE `answers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `answers_pertanyaan_id_foreign` (`pertanyaan_id`),
  ADD KEY `answers_profil_id_foreign` (`profil_id`);

--
-- Indexes for table `comments_answers`
--
ALTER TABLE `comments_answers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `comments_answers_jawaban_id_foreign` (`jawaban_id`),
  ADD KEY `comments_answers_profil_id_foreign` (`profil_id`);

--
-- Indexes for table `comments_questions`
--
ALTER TABLE `comments_questions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `comments_questions_pertanyaan_id_foreign` (`pertanyaan_id`),
  ADD KEY `comments_questions_profil_id_foreign` (`profil_id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indexes for table `like_dislike_answers`
--
ALTER TABLE `like_dislike_answers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `like_dislike_answers_jawaban_id_foreign` (`jawaban_id`),
  ADD KEY `like_dislike_answers_profil_id_foreign` (`profil_id`);

--
-- Indexes for table `like_dislike_questions`
--
ALTER TABLE `like_dislike_questions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `like_dislike_questions_pertanyaan_id_foreign` (`pertanyaan_id`),
  ADD KEY `like_dislike_questions_profil_id_foreign` (`profil_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `profiles`
--
ALTER TABLE `profiles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `questions`
--
ALTER TABLE `questions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `questions_jawaban_tepat_id_foreign` (`jawaban_tepat_id`),
  ADD KEY `questions_profil_id_foreign` (`profil_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `answers`
--
ALTER TABLE `answers`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `comments_answers`
--
ALTER TABLE `comments_answers`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `comments_questions`
--
ALTER TABLE `comments_questions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `like_dislike_answers`
--
ALTER TABLE `like_dislike_answers`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `like_dislike_questions`
--
ALTER TABLE `like_dislike_questions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=108;

--
-- AUTO_INCREMENT for table `profiles`
--
ALTER TABLE `profiles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `questions`
--
ALTER TABLE `questions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `answers`
--
ALTER TABLE `answers`
  ADD CONSTRAINT `answers_pertanyaan_id_foreign` FOREIGN KEY (`pertanyaan_id`) REFERENCES `questions` (`id`),
  ADD CONSTRAINT `answers_profil_id_foreign` FOREIGN KEY (`profil_id`) REFERENCES `profiles` (`id`);

--
-- Constraints for table `comments_answers`
--
ALTER TABLE `comments_answers`
  ADD CONSTRAINT `comments_answers_jawaban_id_foreign` FOREIGN KEY (`jawaban_id`) REFERENCES `answers` (`id`),
  ADD CONSTRAINT `comments_answers_profil_id_foreign` FOREIGN KEY (`profil_id`) REFERENCES `answers` (`id`);

--
-- Constraints for table `comments_questions`
--
ALTER TABLE `comments_questions`
  ADD CONSTRAINT `comments_questions_pertanyaan_id_foreign` FOREIGN KEY (`pertanyaan_id`) REFERENCES `questions` (`id`),
  ADD CONSTRAINT `comments_questions_profil_id_foreign` FOREIGN KEY (`profil_id`) REFERENCES `questions` (`id`);

--
-- Constraints for table `like_dislike_answers`
--
ALTER TABLE `like_dislike_answers`
  ADD CONSTRAINT `like_dislike_answers_jawaban_id_foreign` FOREIGN KEY (`jawaban_id`) REFERENCES `answers` (`id`),
  ADD CONSTRAINT `like_dislike_answers_profil_id_foreign` FOREIGN KEY (`profil_id`) REFERENCES `answers` (`id`);

--
-- Constraints for table `like_dislike_questions`
--
ALTER TABLE `like_dislike_questions`
  ADD CONSTRAINT `like_dislike_questions_pertanyaan_id_foreign` FOREIGN KEY (`pertanyaan_id`) REFERENCES `questions` (`id`),
  ADD CONSTRAINT `like_dislike_questions_profil_id_foreign` FOREIGN KEY (`profil_id`) REFERENCES `questions` (`id`);

--
-- Constraints for table `questions`
--
ALTER TABLE `questions`
  ADD CONSTRAINT `questions_jawaban_tepat_id_foreign` FOREIGN KEY (`jawaban_tepat_id`) REFERENCES `answers` (`id`),
  ADD CONSTRAINT `questions_profil_id_foreign` FOREIGN KEY (`profil_id`) REFERENCES `profiles` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
